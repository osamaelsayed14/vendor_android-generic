# Versioning System For Android-Generic
# Android-Generic RELEASE VERSION
AG_VERSION_MAJOR = 1
AG_VERSION_MINOR = 13
AG_VERSION_MAINTENANCE = Stable

# Android-Generic Android version
AG_VERSION_STATIC = 10.0

#ifdef AG_VERSION_MAINTENANCE
#    VERSION := $(AG_VERSION_MAJOR).$(AG_VERSION_MINOR)-$(AG_VERSION_MAINTENANCE)
#else
    VERSION := $(AG_VERSION_MAJOR).$(AG_VERSION_MINOR)
#endif

TARGET_PRODUCT_SHORT := $(subst AG_,,$(AG_BUILDTYPE))

# Set AG_BUILDTYPE
ifdef AG_NIGHTLY
    AG_BUILDTYPE := NIGHTLY
endif
ifdef AG_EXPERIMENTAL
    AG_BUILDTYPE := EXPERIMENTAL
endif
#ifdef AG_MAINTENANCE
#    AG_BUILDTYPE := MAINTENANCE
#endif
# Set Unofficial if no buildtype set (Buildtype should ONLY be set by Android-Generic Devs!)
ifdef AG_BUILDTYPE
else
    AG_BUILDTYPE := UNOFFICIAL
endif

# Set Android-Generic version
ifdef AG_RELEASE
    AG_VERSION := Android-Generic-v$(VERSION)
else
    AG_VERSION := Android-Generic-v$(VERSION)-$(AG_BUILD)-$(AG_BUILDTYPE)-$(shell date +%Y%m%d)
endif

AG_DISPLAY_VERSION := $(VERSION)
AG_DISPLAY_BUILDTYPE := $(AG_BUILDTYPE)
AG_FINGERPRINT := Android-Generic/$(VERSION)/$(TARGET_PRODUCT_SHORT)/$(shell date +%Y%m%d)

# Versioning System For vendor side of Android-Generic
# Android-Generic RELEASE VERSION
AG_VENDOR_VERSION_MAJOR = $(shell date -u +%Y%m%d)
AG_VENDOR_VERSION_MINOR = $(shell date -u +%H)

# Android-Generic Android version
AG_VENDOR_VERSION := $(AG_VENDOR_VERSION_MAJOR).$(AG_VENDOR_VERSION_MINOR)

